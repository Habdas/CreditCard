package controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import entity.Card;
import repository.CardRepository;
import service.CardInvoker;

@ComponentScan(basePackageClasses = CardInvoker.class)
@ComponentScan(basePackageClasses = ClientRest.class)
@EnableAutoConfiguration
@EnableJpaRepositories(basePackageClasses = CardRepository.class)
@EntityScan(basePackageClasses = Card.class)
public class Main {

	public static void main(String[] args) {
		SpringApplication.run(Main.class,args);
	}

}



