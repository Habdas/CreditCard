package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import entity.Card;
import service.CardInvoker;
import service.DepositTask;
import service.PaymentTask;

@RestController
public class ClientRest {

	@Autowired
	private repository.CardRepository cardRepository;

	@Autowired
	private CardInvoker invoker;

	@Autowired
	private DepositTask depositTask;

	@Autowired
	private PaymentTask paymentTask;

	@RequestMapping(value = "/deposit", method = RequestMethod.POST)
	public ResponseEntity<Void> deposit(@RequestBody RequestObject request) {
		Card cc = cardRepository.findOne(request.getCardNumber());
		depositTask.setAmount(request.getAmount());
		depositTask.setCreditCard(cc);
		invoker.setTask(depositTask);
		invoker.deposit();
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	/*
	 * { "cardNumber": 555, "amount": 1123.44 }
	 */
	@RequestMapping(value = "/pay", method = RequestMethod.POST)
	public ResponseEntity<Void> pay(@RequestBody RequestObject request) {
		Card cc = cardRepository.findOne(request.getCardNumber());
		paymentTask.setAmount(request.getAmount());
		paymentTask.setCreditCard(cc);
		invoker.setTask(paymentTask);
		if (invoker.pay()) {
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
	}
	
	/*
	 * { "cardNumber": 555, "balance": 1123.44, "limitBalance": -1000 }
	 */
	@RequestMapping(value = "/addCard", method = RequestMethod.POST)
	public void getAdd(@RequestBody Card card) {
		cardRepository.save(card);
	}

}
