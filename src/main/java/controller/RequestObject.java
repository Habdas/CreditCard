package controller;

import java.math.BigDecimal;

import org.springframework.web.bind.annotation.PathVariable;

public class RequestObject {
	private Long cardNumber;
	private BigDecimal amount;
	
	public RequestObject() {
		
	}

	public RequestObject(@PathVariable("cardNumber")Long cardNumber,@PathVariable("amount") BigDecimal amount) {
		this.cardNumber = cardNumber;
		this.amount = amount;
	}

	public void setCardNumber(Long cardNumber) {
		this.cardNumber = cardNumber;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getCardNumber() {
		return cardNumber;
	}

	public BigDecimal getAmount() {
		return amount;
	}
}
