package entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Card {
	
	@Id
	private Long cardNumber;

	private BigDecimal balance;
	
	private BigDecimal limitBalance;
	
	
	public BigDecimal getLimitBalance() {
		return limitBalance;
	}

	public void setLimitBalance(BigDecimal limitBalance) {
		this.limitBalance = limitBalance;
	}
	
	public Card() {
		
	}

	public Card(Long cardNumber, BigDecimal balance, BigDecimal limit) {
		this.cardNumber = cardNumber;
		this.balance = balance;
		this.limitBalance = limit;
	}
	
	public Long getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(Long cardNumber) {
		this.cardNumber = cardNumber;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}


}
