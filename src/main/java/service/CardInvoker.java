package service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CardInvoker{
	@Autowired
	private TaskController taskControler;
	
	private Task task;
	
	public CardInvoker() {
	}

	public void deposit() {
		taskControler.setAction(task);
		taskControler.execute();
	}

	public boolean pay() {
		taskControler.setAction(task);
		return taskControler.execute();
	}

	public void backOperation() {
		taskControler.setAction(task);
		taskControler.rollback();
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}
}
