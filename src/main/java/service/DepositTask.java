package service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import entity.Card;
import repository.CardRepository;

@Component
@RequestScope
public class DepositTask implements Task {

	private Card creditCard;
	private BigDecimal amount;

	@Autowired
	private CardRepository repository;

	public DepositTask() {

	}

	public boolean execute() {
		BigDecimal currentBalance = creditCard.getBalance();
		creditCard.setBalance(currentBalance.add(amount));
		repository.save(creditCard);
		System.out.println("Before deposit: " + currentBalance + " after deposit: " + (currentBalance.add(amount)));
		return true;
	}

	public void rollback() {
		// TODO Auto-generated method stub
	}

	public Card getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(Card creditCard) {
		this.creditCard = creditCard;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public CardRepository getRepository() {
		return repository;
	}

	public void setRepository(CardRepository repository) {
		this.repository = repository;
	}

}
