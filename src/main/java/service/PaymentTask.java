package service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import entity.Card;
import repository.CardRepository;

@Component
@RequestScope
public class PaymentTask implements Task {
	private Card creditCard;
	private BigDecimal amount;

	@Autowired
	private CardRepository repository;

	public PaymentTask() {

	}

	public boolean execute() {
		BigDecimal balance = creditCard.getBalance();
		if (balance.subtract(amount).compareTo(creditCard.getLimitBalance()) == 1) {
			creditCard.setBalance(balance.subtract(amount));
			System.out.println("Before payment: " + balance + " after payment: " + balance.subtract(amount));
			System.out.println("Payment SUCCESS :)");
			repository.save(creditCard);
			return true;
		}
		System.out.println("Payment FAILED :(");
		return false;
	}

	public void rollback() {
		// TODO Auto-generated method stub
	}

	public Card getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(Card creditCard) {
		this.creditCard = creditCard;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
