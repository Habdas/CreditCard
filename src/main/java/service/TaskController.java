package service;

import org.springframework.stereotype.Component;

@Component
public class TaskController {

	
	private Task action;

	public void setAction(Task action) {
		this.action = action;
	}

	public boolean execute() {
		return action.execute();
	}

	public void rollback() {
		action.rollback();
	}
}
