package service;

public interface Task {
	public boolean execute();
	public void rollback();
}
